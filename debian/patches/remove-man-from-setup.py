Description: Remove wapiti.1.gz from setup.py
  Using debian/wapiti.1 as manpage, because it contains typos fixes.


Author: Gianfranco Costamagna <costamagnagianfranco@yahoo.it>
Origin: debian
Forwarded: not-needed
Last-Update: 2014-09-08

--- a/setup.py
+++ b/setup.py
@@ -13,7 +13,7 @@
                             "README",
                             "TODO",
                             "VERSION"]))
-doc_and_conf_files.append(("share/man/man1", ["doc/wapiti.1.gz"]))
+#doc_and_conf_files.append(("share/man/man1", ["doc/wapiti.1.gz"]))
 
 # Main
 setup(
